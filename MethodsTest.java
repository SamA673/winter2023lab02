public class MethodsTest{
	public static void main(String [] args){
		int x = 5; 
		System.out.println("The value of x is: " + x); 
		System.out.println(); 


		// Method 1
		System.out.println("______________________________________"); 
		System.out.println(); 
		System.out.println("The value of x before the NoInputNoReturn method is " + x); 
		NoInputNoReturn(); 
		System.out.println("The value of x after the NoInputNoReturn method is " + x); 


		// Method 2
		System.out.println("______________________________________"); 
		System.out.println();
		System.out.println("The value of x before calling methodOneInputNoReturn is " + x); 
		methodOneInputNoReturn(x+10); 
		System.out.println("The value of x after calling methodOneInputNoReturn is " + x);


		// Method 3 
		System.out.println("______________________________________"); 
		System.out.println(); 
		methodTwoInputNoReturn(10, 1.23); 
		
		// Method 4 
		System.out.println("______________________________________"); 
		System.out.println(); 
		int m4 = methodNoInputReturnInt(); 
		System.out.println( "The value of the m4 int is: " + m4 ); 

		// Method 5
		System.out.println("______________________________________"); 
		System.out.println(); 
		double m5Root = sumSquareRoot(5, 9); 
		System.out.println("The value of m5root is: " + m5Root); 
		
		
		// Number 10 
		System.out.println("______________________________________"); 
		System.out.println();
		String s1 = "java"; 
		String s2 = "programming"; 
		System.out.println("s1 has a length of: " + s1.length() ); 

		// SecondClass Method 1  
		System.out.println("______________________________________"); 
		System.out.println();
		System.out.println("addOne returns: " + SecondClass.addOne(50));
		
		// SecondClass Method 2
		System.out.println("______________________________________"); 
		System.out.println();
		SecondClass class2 = new SecondClass(); 
		System.out.println("addTwo returns: " + class2.addTwo(50));
		
		
		
	} 
	
	public static void NoInputNoReturn(){

		System.out.println("I’m in a method that takes no input and returns nothing"); 
		int x = 20; 
		System.out.println("The value of x within the NoInputNoReturn method is: "  + x); 


	
	}
	
	public static void methodOneInputNoReturn(int y){
		y -= 5; 
		System.out.println("Inside the methodOneInputNoReturn");
		System.out.println("The value of y is " + y); 
		System.out.println(); 

		
		
	}
	
	
	
	public static void methodTwoInputNoReturn(int a, double b){
		System.out.println("The value of the a integer is: " + a); 
		System.out.println("The value of the b double is: " + b); 
		
		
		
	}
	
	public static int methodNoInputReturnInt(){
		return 5; 
		
	}
	
	
	public static double sumSquareRoot(int numberA, int numberB){
		int sum = numberA + numberB; 
		return Math.sqrt(sum);
		
	}
	
	
} 