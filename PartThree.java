import java.util.Scanner; 



public class PartThree{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in); 
		
		System.out.println("Enter value A: ");
		double a = scan.nextDouble(); 
		
		System.out.println("Enter value B: ");
		double b = scan.nextDouble(); 
	

		
		// Static methods
		System.out.println("The sum of a and b is: " + Calculator.addNums(a,b)); 
		System.out.println("The difference of a and b is " + Calculator.subNums(a,b));		
	
		// Instance methods 
		Calculator calc = new Calculator();
		System.out.println("The multiplication of a and b results in: " + calc.multiplyNums(a,b));
		System.out.println("The division of a and b results in: " + calc.divideNums(a,b)); 
		
		
	} 
} 